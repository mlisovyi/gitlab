---
redirect_to: '../user/project/repository/push_rules.md'
remove_date: '2022-05-10'
---

This document was moved to [another location](../user/project/repository/push_rules.md).

<!-- This redirect file can be deleted after <2022-05-10>. -->
<!-- Before deletion, see: https://docs.gitlab.com/ee/development/documentation/#move-or-rename-a-page -->
